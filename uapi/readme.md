## Copyright text

    Copyright (c) $today.year. The UAPI Authors
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at the LICENSE file.

    You must gained the permission from the authors if you want to
    use the project into a commercial product

## Module ID rule

 * Module ID construct by 4 hex digits.
 * All modules in Base repository will use 0x0010 to 0x00ff as its id.
 * All modules in Cornerstone repository will use 0x0100 to 0x01ff as its id.
 * All modules in Facility repository will use 0x0200 to 0x02ff as its id.
 * All user modules will use 0x1000 to 0xffff as its id.

### Base module id mapping

    uapi.annotation         0x0010
    uapi.codegen            0x0011
    uapi.common             0x0012
    uapi.exception          0x0013
    uapi.state              0x0014

### Cornerstone module id mapping

    uapi.service            0x0100
    uapi.log                0x0101
    uapi.config             0x0102
    uapi.event              0x0103
    uapi.behavior           0x0104
    uapi.app                0x0105
    uapi.command            0x0106
    uapi.utm                0x0107

### Facility module id mapping:
    uapi.app.terminal       0x0200
    uapi.auth               0x0201
    uapi.resource           0x0202
    uapi.user               0x0203
    uapi.service.spring     0x0204
    uapi.behavior.common    0x0205
    uapi.net                0x0210
    uapi.net.telnet         0x0211
    uapi.net.http           0x0212
    uapi.net.http.netty     0x0213
    uapi.protocol           0x0220
    uapi.protocol.graphql   0x0221

## Debugging

To debug at gradle build-time, just create java remote debug connect to 5005 port.